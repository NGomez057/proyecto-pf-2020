FROM node:14.9.0
ENV NODE_ENV production
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY ./build ./
RUN npm install
USER node
EXPOSE 3000
CMD ["node",  "./app.js"]