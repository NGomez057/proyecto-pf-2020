import { ObjectID } from 'mongodb';
import { Utils } from '../service/Utils';

export class Participante {
  public id: ObjectID;
  public permisos: ('CREATE' | 'DELETE')[];

  public copy(obj: any, keys?: (keyof Participante)[]) {
    if (obj == null) {
      return this;
    }

    if (keys != null) {
      Utils.copyToObj(this, keys, obj);
    } else {
      Utils.copyToObj(this, Participante.Keys, obj);
    }

    if (this.id != null) {
      this.id = new ObjectID(this.id);
    }

    return this;
  }
}

export namespace Participante {
  export enum Keys {
    ID = 'id',
    PERMISOS = 'permisos'
  }
}
