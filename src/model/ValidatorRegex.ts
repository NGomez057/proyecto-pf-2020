export class ValidatorRegex {
  static get EMAIL() {
    return /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
  }

  static get PASSWORD() {
    return /^[a-zA-Z][a-zA-Z0-9.]{3,19}$/;
  }

  static get NAME() {
    return /^[a-zA-Z ]+$/;
  }
}
