import { ObjectID } from 'mongodb';
import { User } from '../model/User';
import { Utils } from '../service/Utils';
import { MongoConnection } from './MongoConnection';

export class UserRepository {
  private db: MongoConnection;
  private COLLECTION = 'user';

  public async createUser(user: User) {
    return await this.db.insert(this.COLLECTION, user);
  }

  public async getUserByEmail(email: string) {
    const query = { email: email };

    const res = await this.db.find(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }

    return new User().copy(res[0]);
  }

  public async getUserById(id: ObjectID) {
    const query = { _id: id };

    const res = await this.db.find(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new User().copy(res[0]);
  }

  public async getUserByEmailOrNameOrSurname(
    searchParam: string,
    idUser: ObjectID
  ) {
    const out: User[] = [];
    const regex = new RegExp(Utils.escapeRegExp(searchParam));
    const res = await this.db.find<User>(this.COLLECTION, {
      $and: [
        { $or: [{ email: regex }, { name: regex }, { surname: regex }] },
        { _id: { $ne: idUser } }
      ]
    });

    for (const user of res) {
      const aux = new User().copy(user, ['_id', 'name', 'surname', 'email']);
      delete aux.emailCheck;
      out.push(aux);
    }

    return out;
  }

  public async getUserByVerificationCode(code: string) {
    const query = { verificationCode: code };

    const res = await this.db.find<User>(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new User().copy(res[0]);
  }

  public async isVerificationCodeUser(code: string) {
    const query = { verificationCode: code };

    const res = await this.db.find<User>(this.COLLECTION, query, { _id: 1 });
    return res.length > 0;
  }

  public async updateUser(user: User) {
    return await this.db.update(this.COLLECTION, user);
  }

  // ------------------- SINGLETON --------------------
  private static instance: UserRepository;
  private constructor() {}

  private async init() {
    this.db = await MongoConnection.getInstance();
  }

  static async getInstance() {
    if (UserRepository.instance == null) {
      UserRepository.instance = new UserRepository();
      await UserRepository.instance.init();
    }

    return UserRepository.instance;
  }
}
