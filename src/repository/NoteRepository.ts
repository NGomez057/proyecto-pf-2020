import { ObjectID } from 'mongodb';
import { Note } from '../model/Note';
import { MongoConnection } from './MongoConnection';

export class NoteRepository {
  private db: MongoConnection;
  private COLLECTION = 'nota';

  public async createNote(note: Note) {
    return await this.db.insert(this.COLLECTION, note);
  }

  public async getNotesByTituloAndIdLibreta(titulo: string, id_libreta: ObjectID) {
    const query = {
      titulo: titulo,
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Note().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'checklist'])
      );
    }

    return out;
  }

  public async getNotesByTituloAndIdCreatos(titulo: string, id_creador: ObjectID) {
    const query = {
      titulo: titulo,
      id_creador: id_creador
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Note().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'checklist'])
      );
    }

    return out;
  }

  public async getNotesByUserAndIdLibreta(id_creador: ObjectID, id_libreta: ObjectID | null) {
    const query = {
      id_creador: id_creador,
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Note().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'checklist'])
      );
    }

    return out;
  }

  public async getSharedNotesByUser(id_creador: ObjectID) {
    const query = {
      'participantes.id': id_creador,
      id_libreta: null
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Note().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'checklist'])
      );
    }

    return out;
  }

  // ------------------- SINGLETON --------------------
  private static instance: NoteRepository;
  private constructor() {}

  private async init() {
    this.db = await MongoConnection.getInstance();
  }

  static async getInstance() {
    if (NoteRepository.instance == null) {
      NoteRepository.instance = new NoteRepository();
      await NoteRepository.instance.init();
    }

    return NoteRepository.instance;
  }
}
