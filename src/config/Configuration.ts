export class Configuration {
  public static UNAUTHORIZED_URL: UrlPermission[] = [
    { url: '/api/login', method: 'POST' },
    { url: '/api/usuario', method: 'POST' },
    { url: '/api/change-password', method: 'POST' },
    { url: '/api/restore-password', method: 'POST' }
  ];
}

export interface UrlPermission {
  url: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
}
