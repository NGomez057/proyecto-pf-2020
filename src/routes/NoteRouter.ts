import { Request, Response, Router } from 'express';
import { StatusCodes } from 'http-status-codes';
import { NoteBusiness } from '../business/user/NoteBusiness';
import { ApiRouter } from '../model/ApiRouter';

export class NoteRoute extends ApiRouter {
  public libretaBusiness: NoteBusiness;

  private async createNote(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const id = await this.libretaBusiness.createNote(req.body, token);
    res.status(StatusCodes.CREATED).send({ id: id });
  }

  private async getOwnNotes(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const notes = await this.libretaBusiness.getOwnNotes(token);
    res.status(StatusCodes.OK).send(notes);
  }

  private async getSharedNotes(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const notes = await this.libretaBusiness.getSharedNotes(token);
    res.status(StatusCodes.OK).send(notes);
  }

  // ------------------- SINGLETON --------------------
  private static instance: NoteRoute;
  private constructor() {
    super();
  }

  private async init() {
    this.libretaBusiness = await NoteBusiness.getInstance();

    // Manejo de operaciones /api/libreta
    this.router.post('/', (req, res, next) => {
      this.createNote(req, res).catch(next);
    });

    this.router.get('/', (req, res, next) => {
      this.getOwnNotes(req, res).catch(next);
    });

    this.router.get('/shared', (req, res, next) => {
      this.getSharedNotes(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (NoteRoute.instance == null) {
      NoteRoute.instance = new NoteRoute();
      await NoteRoute.instance.init();
    }

    return NoteRoute.instance;
  }
}
