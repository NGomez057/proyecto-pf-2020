// lib/app.ts
import express = require('express');
import { StatusCodes } from 'http-status-codes';
import 'reflect-metadata';
import { BusinessException } from './Exceptions/BusinessException';
import { AuthorizationFilter } from './filter/AuthorizationFilter';
import { ApiRoute } from './routes/ApiRouter';

import e = require('express');

// Create a new express application instance
const app: express.Application = express();

console.clear();
// let u = Utils.copyToObj(User, { username: 'root' });
// u = Utils.copyToObj(User, { password: 1 }, u);
// console.log(u);

async function init() {
  // MONGO
  // let m = await MongoConnection.getInstance();
  // console.log(
  //   await m.find('user', {
  //     _id: ObjectID.from('5f9865f07509811fd0998eee'),
  //   })
  // );
  //
  // TOKEN
  // const token = JWT.generateJWT({ user: 'nico' });
  // console.log(token);
  // console.log(JWT.decode(token));
  // setTimeout(() => {
  //   console.log(JWT.decode(token));
  // }, 2000);
  //
  // EMAIL
  // const email = Email.getInstance();
  // console.log(await email.sendMail('nicoantogd@gmail.com', 'Test', 'Hola'));

  // CORS
  // app.use()

  // Parseo de body
  app.use(express.json());

  // Establenciendo el filtro por token
  app.use(AuthorizationFilter);

  const api = await ApiRoute.getInstance();
  app.use('/api', api.getRouter());

  // Manejando errores
  app.use((err: Error, req: express.Request, res: express.Response, next) => {
    if (err instanceof BusinessException) {
      res.status(err.resCode).send(err.msg);
      return;
    }

    console.error(err.message, '\n' + err.stack);

    res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
  });

  // Iniciando el server
  app.listen(3000, () => {
    console.log('Example app listening on port 3000!');
  });
}

init();
