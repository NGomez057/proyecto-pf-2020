import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { BusinessException } from '../../Exceptions/BusinessException';
import { Note } from '../../model/Note';
import { Notebook } from '../../model/Notebook';
import { User } from '../../model/User';
import { NotebookRepository } from '../../repository/NotebookRepository';
import { NoteRepository } from '../../repository/NoteRepository';
import { UserRepository } from '../../repository/UserRepository';
import { JWT } from '../../service/jwt/Jwt';
import { Utils } from '../../service/Utils';

export class NoteBusiness {
  private repository: NoteRepository;
  private userRepository: UserRepository;
  private notebookRepository: NotebookRepository;

  /**
   * @throws BusinessException
   */
  public async createNote(note: Note, token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const user = await this.userRepository.getUserByEmail(email);
    if (user == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    let copyNote: Note;
    try {
      copyNote = new Note().copy(note, ['titulo', 'id_libreta']);
    } catch (error) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid notebook id');
    }
    copyNote.id_creador = user._id;

    await this.validateNote(copyNote);

    let notesDB: Note[];
    let notebook: Notebook | null = null;
    if (copyNote.id_libreta != null) {
      // Validar si tiene permisos en la libreta
      notebook = await this.notebookRepository.getNotebookById(copyNote.id_libreta);
      if (notebook == null) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Notebook not exists');
      }

      // Si no es mi libreta, revisar si tengo permiso
      if (!notebook.id_creador.equals(user._id)) {
        const participants = notebook.participantes;
        if (participants == null) {
          throw new BusinessException(StatusCodes.FORBIDDEN);
        }

        // Obtener mis permisos en la libreta
        const myPermission = Utils.searchObjInArray(
          participants,
          'id',
          user._id,
          (e: ObjectID, v: ObjectID) => {
            return e.equals(v);
          }
        ).obj;

        // Si no estoy como participante o no tengo permisos
        if (myPermission == null || myPermission.permisos == null) {
          throw new BusinessException(StatusCodes.FORBIDDEN);
        }

        // Si tengo el permiso CREATE
        const hasPermission = myPermission.permisos.find((e) => e === 'CREATE') != null;
        if (hasPermission === false) {
          throw new BusinessException(StatusCodes.FORBIDDEN);
        }
      }

      notesDB = await this.repository.getNotesByTituloAndIdLibreta(
        copyNote.titulo,
        copyNote.id_libreta
      );
    } else {
      notesDB = await this.repository.getNotesByTituloAndIdCreatos(copyNote.titulo, user._id);
    }
    if (notesDB.length > 0) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Note already exists');
    }

    const id = await this.repository.createNote(copyNote);

    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    return id;
  }

  public async getOwnNotes(token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const user = await this.userRepository.getUserByEmail(email);
    if (user == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const notes = await this.repository.getNotesByUserAndIdLibreta(user._id, null);

    return notes;
  }

  public async getSharedNotes(token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const user = await this.userRepository.getUserByEmail(email);
    if (user == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const libretas = await this.repository.getSharedNotesByUser(user._id);
    return libretas;
  }

  /**
   * @throws BusinessException
   */
  private async validateNote(note: Note) {
    // Validar Titulo
    if (note.titulo == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad body, Required {titulo}');
    }

    // Validar Descripcion
    if (note.descripcion != null) {
      if (typeof note.descripcion !== 'string') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: must be string');
      }

      if (note.descripcion === '') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: it cant be empty');
      }
    }

    // Validar participantes
    if (note.participantes != null) {
      let user: User | null;
      for (const participant of note.participantes) {
        if (participant.id == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            'Bad participant ' +
              participant.id +
              '\n' +
              'Required {id: string, permisos?: (CREATE | DELETE)[]}'
          );
        }

        if (note.id_creador.equals(participant.id)) {
          throw new BusinessException(StatusCodes.BAD_REQUEST, `You cannot be a participant`);
        }

        // Validar permisos
        const permisos = participant.permisos;
        if (permisos != null) {
          if (!Array.isArray(permisos)) {
            throw new BusinessException(StatusCodes.BAD_REQUEST, 'Permissions have to be a list');
          }
          for (const p of permisos) {
            if (p !== 'CREATE' && p !== 'DELETE') {
              throw new BusinessException(
                StatusCodes.BAD_REQUEST,
                'Permission have to be CREATE or DELETE'
              );
            }
          }
        }

        user = await this.userRepository.getUserById(participant.id);
        if (user == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            `participan not found: ${participant.id}`
          );
        }
      }
    }
  }

  // ----------------- SINGLETON ---------------------
  private static instance: NoteBusiness;
  private constructor() {}

  private async init() {
    this.repository = await NoteRepository.getInstance();
    this.userRepository = await UserRepository.getInstance();
    this.notebookRepository = await NotebookRepository.getInstance();
  }

  static async getInstance() {
    if (NoteBusiness.instance == null) {
      NoteBusiness.instance = new NoteBusiness();
      await NoteBusiness.instance.init();
    }

    return NoteBusiness.instance;
  }
}
