import { compareSync, hashSync } from 'bcrypt';

export class PasswordEncoder {
  private static saltRound = 10;

  public static hashPassword(password: string) {
    return hashSync(password, this.saltRound);
  }

  public static comparePassword(password: string, hash: string) {
    return compareSync(password, hash);
  }
}
