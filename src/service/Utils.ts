export class Utils {
  /**
   * Metodo que se utiliza para transformar un
   * objeto plano en un objeto de una clase.
   * Las propiedades que se copiarán deben estar
   * inicializadas en el objeto a copiar.
   * Si la propiedad no esta en el objeto plano,
   * esta no se copiará.
   *
   * NO COPIA DE FOMRA RECURSIVA
   * SOLO COPIA EL OBJETO DE PRIMER NIVEL
   */
  static copyToObj<T>(obj: T, enumKeys: any, objToCopy, sufix?): T {
    const keys: string[] = Object.values(enumKeys);

    for (const key of keys) {
      if (objToCopy[key] == null) {
        continue;
      }

      obj[key] = objToCopy[key];

      if (sufix != null) {
        obj[key] = obj[key][sufix];
      }
    }

    return obj;
  }

  static copyToArrayObj<T>(Type: new () => T, enumKeys: any, arrToCopy: any[], sufix?): T[] {
    const out: T[] = [];
    for (const elem of arrToCopy) {
      out.push(this.copyToObj(new Type(), enumKeys, elem, sufix));
    }
    return out;
  }

  static searchObjInArray<T>(
    arr: T[],
    field: keyof T,
    value: any,
    cmp = (elem: any, v: any) => {
      return elem === v;
    }
  ) {
    let element;
    for (let i = 0; i < arr.length; i++) {
      element = arr[i][field];

      if (element && cmp(element, value)) {
        return {
          obj: arr[i],
          index: i
        };
      }
    }
    return {
      obj: null,
      index: -1
    };
  }
  static searchDeepObjInArray<T>(arr: T[], field: string, value: any) {
    let elem;
    for (let i = 0; i < arr.length; i++) {
      elem = Utils.getElemByPath(arr[i], field);

      if (elem && elem === value) {
        return {
          obj: arr[i],
          index: i
        };
      }
    }
    return {
      obj: null,
      index: -1
    };
  }

  private static getElemByPath<T>(obj: T, path: string) {
    const paths = path.split('.');
    let elem = obj;
    for (let i = 0; i < paths.length; i++) {
      elem = elem[paths[i]];
    }

    return elem;
  }

  static getRandomHash(length) {
    const charset = 'Aa0Bb1Cc2Dd3Ee4Ff5Gg6Hh7Ii8Jj9KkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz';
    let result = '';
    for (let i = 0; i < length; i++) result += charset[~~(Math.random() * charset.length)];
    return result;
  }

  static escapeRegExp(regexStr: string) {
    return regexStr.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }
}
