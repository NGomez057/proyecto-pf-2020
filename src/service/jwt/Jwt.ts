import * as jwt from 'jsonwebtoken';

export class JWT {
  static key = 'pf2020JwtKey';

  public static generateJWT(payload: any) {
    return jwt.sign(payload, JWT.key, { expiresIn: 3600 });
  }

  public static decode(token: string) {
    try {
      return jwt.verify(token, JWT.key) as any;
    } catch (e) {
      return null;
    }
  }
}
