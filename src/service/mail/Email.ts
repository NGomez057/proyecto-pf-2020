import * as mail from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';

export class Email {
  private tranport: Mail;
  private user = 'no.reply.pf2020.api@gmail.com';
  private password = 'PF_2020_API_MAIL';

  public sendMail(to: string, title: string, body: string) {
    return this.tranport.sendMail({
      from: this.user,
      to,
      subject: title,
      text: body
    });
  }

  public sendMailHTML(to: string, title: string, body: string) {
    return this.tranport.sendMail({
      from: this.user,
      to,
      subject: title,
      html: body
    });
  }

  // ------------------- SINGLETON --------------------
  private static instance: Email;

  private constructor() {
    this.tranport = mail.createTransport({
      service: 'gmail',
      auth: {
        user: this.user,
        pass: this.password
      }
    });
  }

  static getInstance() {
    if (Email.instance == null) {
      Email.instance = new Email();
    }

    return Email.instance;
  }
}
